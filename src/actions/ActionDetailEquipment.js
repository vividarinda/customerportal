import axios from 'axios';
import Environment from '../views/Environment/Environment';
import { LIST_DETAIL_EQUIPMENT, LIST_DETAIL_EQ_CAUTION } from '../values/type.js';
import { AsyncStorage } from 'AsyncStorage';

export const getDetailEquipment= (equipmentId) => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternalProd + '/EM/api/equipment/' + equipmentId, {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternalProd, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    let dataDetail = {
                        equipmentId : response['data'].equipmentId,
                        imagePath: response['data'].imagePath,
                        serialNumber : response['data'].serialNumber,
                        unitCode : response['data'].unitCode,
                        modelNumber : response['data'].modelNumber,
                        warrantyEndDate : response['data'].warrantyEndDate,
                        warrantyStatusString : response['data'].warrantyStatusString,
                        smrTotalInMinutes : response['data'].smrTotalInMinutes,
                        smrLastValueDate : response['data'].smrLastValueDate,
                        valueFuelConsumption : response['data'].characteristicGroups[1].characteristics[0].value,
                        valueWorkingHours : response['data'].characteristicGroups[2].characteristics[0].value
                    }
                    dispatch({ type: LIST_DETAIL_EQUIPMENT, payload: dataDetail });              
                }))
        })
    }
}

export const getDetailEqCaution = (equipmentId) => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternalProd + '/EM/api/equipment/' + equipmentId + '/caution', {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternalProd, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    let stepCau = Array();
                    let data = {
                        cautionId : response['data'][0]['cautionId'],
                        description : response['data'][0]['description'],
                        cautionStepId : stepCau
                    }

                    for (var i = 0; i < response['data'][0]['cautionSteps'].length; i++)
                    {
                        let cau = {
                            cautionStepId : response['data'][0]['cautionSteps'][i]['cautionStepId'],
                            descriptionStep : response['data'][0]['cautionSteps'][i]['description'],
                            sequence : response['data'][0]['cautionSteps'][i]['sequence']
                        }
                        stepCau.push(cau);
                    }
                    console.log(data);
                    dispatch({ type: LIST_DETAIL_EQ_CAUTION, payload:data });              
                }))

        })
    }
}