import axios from 'axios';
import Environment from '../views/Environment/Environment';
import { LIST, LIST_SUCCESS, LIST_FAILED, LIST_SUCCESS_BY_TYPE } from '../values/type.js';
import { AsyncStorage } from 'AsyncStorage';

export const getEquipmentAll = () => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternal + '/EM/api/equipment/dasboard', {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternal, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    var list = [];
                    for (var i = 0; i < response['data']['data'].length; i++) {
                        let dataEquipment = {
                            brandCode: response['data']['data'][i]['brandCode'],
                            brandName: response['data']['data'][i]['brandName'],
                            brandImagePath: response['data']['data'][i]['brandImg'],
                            sequence: response['data']['data'][i]['sequence'],
                            numberOfNewCaution: response['data']['data'][i]['numbOfNewCaution'],
                            amount: response['data']['data'][i]['amount'],
                            isCaution: response['data']['data'][i]['isCaution']
                        }
                        list.push(dataEquipment);
                    }
                    dispatch({ type: LIST_SUCCESS, payload: list })

                }))

        })
    }

}

export const getEquipmentsByType = (brandCode) => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternal + '/EM/api/equipment/dashboard?customerCode=' + brandCode, {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternal, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    dispatch({ type: LIST_SUCCESS_BY_TYPE, payload: response['data'] })

                }
                )

                )

        })
    }
}
