import axios from 'axios';
import Environment from '../views/Environment/Environment';
import { LIST_ORDER_SUCCESS, LIST_ORDER_SUMMARY, LIST_CLOSED_ORDER} from '../values/type.js';
import { AsyncStorage } from 'AsyncStorage';

export const getOrder= () => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternalProd + '/OT/api/order/openorder', {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternalProd, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    var list = [];
                    for (var i = 0; i < response['data']['data'].length; i++) {
                        let dataOrder = {
                            poCustomer: response['data']['data'][i]['poCustomer'],
                            customerName: response['data']['data'][i]['customerName'],
                            plantName: response['data']['data'][i]['plantName'],
                            soNumber: response['data']['data'][i]['soNumber'],
                            maxETA: response['data']['data'][i]['maxETA'],
                            soDate: response['data']['data'][i]['soDate'],
                            totalLineItem: response['data']['data'][i]['totalLineItem'],
                            availabilityPercentage: response['data']['data'][i]['availabilityPercentage']
                        }
                        list.push(dataOrder);
                    }
                    dispatch({ type: LIST_ORDER_SUCCESS, payload: list })
                }))
        })
    }
}

export const getClosedOrder= () => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternalProd + '/OT/api/order/closedorder', {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternalProd, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    var list = [];
                    for (var i = 0; i < response['data']['data'].length; i++) {
                        let dataOrder = {
                            poCustomer: response['data']['data'][i]['poCustomer'],
                            soNumber: response['data']['data'][i]['soNumber'],
                            deliveredDate: response['data']['data'][i]['deliveredDate'],
                            soDate: response['data']['data'][i]['soDate'],
                            reviewStar: response['data']['data'][i]['reviewStar'],
                            otifPercentage: response['data']['data'][i]['otifPercentage']
                        }
                        list.push(dataOrder);
                    }
                    dispatch({ type: LIST_CLOSED_ORDER, payload: list })
                }))
        })
    }
}

export const getOrderSummary= () => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternalProd + '/OT/api/order/ordersummaryfordashboard', {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternalProd, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    console.log(response['data']);
                    dispatch({ type: LIST_ORDER_SUMMARY, payload: response['data'] })

                }))

        })
    }

}