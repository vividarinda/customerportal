import axios from 'axios';
import Environment from '../views/Environment/Environment';
import { LIST, LIST_SUCCESS, LIST_FAILED, LIST_SUCCESS_BY_TYPE } from '../values/type.js';
import { AsyncStorage } from 'AsyncStorage';

export const getDetailEquipmentall= () => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternal + '/EM/api/equipment' +equipmentId, {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternal, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                    var list = [];
                    for (var i = 0; i < response['data']['data'].length; i++) {
                        let dataDetailEquipment = {
                            equipmentId: response['data']['data'][i]['equipmentId'],
                            serialNumber: response['data']['data'][i]['serialNumber'],
                            modelNumber: response['data']['data'][i]['modelNumber'],
                            unitCode: response['data']['data'][i]['unitCode'],
                            customerName: response['data']['data'][i]['customerName'],
                            plantDescription: response['data']['data'][i]['plantDescription'],
                            imagePath: response['data']['data'][i]['imagePath'],
                            isUnitCaution: response['data']['data'][i]['isUnitCaution'],
                            isTransmitted: response['data']['data'][i]['isTransmitted'],
                            lastTransmitted: response['data']['data'][i]['lastTransmitted'],
                            operationStatus: response['data']['data'][i]['operationStatus'],
                            operationStatusString: response['data']['data'][i]['operationStatusString'],
                            warrantyEndDate: response['data']['data'][i]['warrantyEndDate'],
                            warrantyStatus: response['data']['data'][i]['warrantyStatus'],
                            warrantyStatusString: response['data']['data'][i]['warrantyStatusString'],
                            fuelConsumptionPerDay: response['data']['data'][i]['fuelConsumptionPerDay'],
                            smrValuePerDayInMinutes: response['data']['data'][i]['smrValuePerDayInMinutes'],
                            smrTotalInMinutes: response['data']['data'][i]['smrTotalInMinutes'],
                            smrLastValueDate: response['data']['data'][i]['smrLastValueDate'],
                            smrType: response['data']['data'][i]['smrType'],
                            smrTypeID: response['data']['data'][i]['smrTypeID']

                        }
                        list.push(dataDetailEquipment);
                    }
                    dispatch({ type: LIST_SUCCESS, payload: list })

                }))

        })
    }

}

export const getDetailEquipmentsByType = (equipmentId) => {
    return (dispatch) => {
        AsyncStorage.getItem('dataLogin').then((result) => {
            let resultparse = JSON.parse(result);
            const token = resultparse.accessToken;
            const expiresIn = resultparse.expiresIn;

            return axios.get(Environment.baseInternal + '/EM/api/equipment' + equipmentId, {
                headers: {
                    "x-ibm-client-id": Environment.XIBMInternal, "Authorization": "Bearer " + token
                }
            })
                .then((response => {
                        dispatch({ type: LIST_SUCCESS_BY_TYPE, payload: response['data']})

                }
                
                
            )
            
        )

        })
    }
}
