import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';
import './DefaultHeader.style.css';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg';
import sygnet from '../../assets/img/brand/sygnet.svg';
import { AsyncStorage } from 'AsyncStorage';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: ""
    }

  }

  componentWillMount() {
    AsyncStorage.getItem('dataLogin').then((result) => {
      let resultparse = JSON.parse(result);
      this.setState({ username : resultparse.userName})
      console.log(this.state.username);
    });
  }

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    return (
      <React.Fragment>
        {/* <AppSidebarToggler className="d-lg-none" display="md" mobile /> */}
        <img src={'../../assets/img/Vector.png'} className="zindex" height="54" width="270" />

        <span className="spanLogo">
          <img src={'../../assets/img/logoUnitedTractor.png'} width="180" />
        </span>
        {/* <AppNavbarBrand
                    full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
                    minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
                  /> */}
        {/* <AppSidebarToggler className="d-md-down-none" display="lg" /> */}
        <Nav className="d-md-down-none paddingmenu" navbar>
          <NavItem className="px-3">
            <NavLink to="/home" className="nav-link"><img src={'../../assets/img/iconHomeActive.png'} width="13" height="15" className="paddingnavitem" />Home</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="/equipment" className="nav-link"><img src={'../../assets/img/iconEquipmentNonActive.png'} width="17" height="15" className="paddingnavitem" />Equipment</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/order" className="nav-link"><img src={'../../assets/img/iconOrderNonActive.png'} width="14" height="16" className="paddingnavitem" />Order</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link"><img src={'../../assets/img/iconNotificationNonActive.png'} width="13" height="15" className="paddingnavitem" />Notification</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-bell"></i>
              <span className="badge badge-danger badge-pill badgeNotif">5</span>
              <span className="notifMenu">Notification</span></NavLink>
          </NavItem> */}
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-bell"></i>Equipment</NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-bell"></i>Order</NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-bell"></i>Other</NavLink>
          </NavItem> */}
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-list"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-location-pin"></i></NavLink>
          </NavItem> */}
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/profile.png'} className="img-avatar" alt="vividarinda@gmail.com" />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center"><strong>{this.state.username}</strong></DropdownItem>
              <DropdownItem><i className="fa fa-phone-square"></i> Contact Us</DropdownItem>
              {/* <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem> */}
              {/* <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem> */}
              {/* <DropdownItem divider /> */}
              <DropdownItem onClick={e => this.props.onLogout(e)}><i className="fa fa-sign-out"></i> Logout</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>


    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
