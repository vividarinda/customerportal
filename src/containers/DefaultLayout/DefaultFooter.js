import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Container } from 'reactstrap';
import './DefaultFooter.style.css';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <div className="widthfooter">
        <Row className="firstfooter">
          <Col sm="3" className="col-s-6">
            <img src={'../../assets/img/logoUnitedTractor.png'} width="150" className="spanfirstfooter"/>
            <p className="fontfooter">Distributor peralatan berat terbesar dan terkemuka di Indonesia yang
              menyediakan produk-produk dari merk ternama dunia seperti Komatsu, 
              UD Trucks, Scania, Bomag, Tadano, dan Komatsu Forest.
            </p>
          </Col>
          <Col sm="3" className="col-s-6">
             <p className="spanfirstfooter marginfooter">Customer Portal</p>
             <p className="fontfooter marginfooter"> Equipment </p>
             <p className="fontfooter marginfooter"> Order </p>
             <p className="fontfooter marginfooter"> Notification </p>
             <p className="fontfooter marginfooter"> Other </p>
             <p className="fontfooter marginfooter"> About </p>
          </Col>
          <Col sm="3" className="col-s-6">
              <p className="spanfirstfooter marginfooter">Get in Touch</p>
              <p className="fontfooter marginfooter"> Telepon : (021) 2457999</p>
              <p className="fontfooter marginfooter"> Website : http://www.unitedtractors.com </p>
              <p className="fontfooter marginfooter"> Alamat : Cakung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13910 </p>
          </Col>
          <Col sm="3" className="col-s-6">
              <p className="spanfirstfooter marginfooter">Follow us</p>
              <p className="marginfooter">
                <img src={'../../assets/img/facebook.png'} width="20" className="paddingimagefooter" />
                <img src={'../../assets/img/instagram.png'} width="20" className="paddingimagefooter" />
                <img src={'../../assets/img/twitter.png'} width="20" className="paddingimagefooter"/>
                <img src={'../../assets/img/line.png'} width="20" />
              </p>
          </Col>
        </Row>
        <Row>
          <Col className="footerStyle">
            <Row>
              <Col className="col-s-12 col-6">
                <span>&copy; 2019 Customer Portal - All rights reserved.</span>
              </Col>

              <Col className="col-s-12 col-6 textalign">
                <span className="spanprivacyfooter">Privacy</span>
                <span className="spanprivacyfooter">Terms & Conditions</span>
                <span className="spanprivacyfooter">FAQ</span>
              </Col>
            </Row>
            {/* <span className="ml-auto">Privacy</span> */}
          </Col>
        </Row>    
        </div>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
