module.exports = {
    baseInternal :'https://gateway-dev.unitedtractors.com/dev/internal',
    baseCustomer : 'https://gateway-dev.unitedtractors.com/dev/customer/',

    baseInternalProd : 'https://gateway.unitedtractors.com/cst/internal',

    XIBMInternal : 'cbd4e2d2-5076-4c99-93f7-56a768af0a88',
    XIBMCustomer : '84264fca-5c3a-479c-9950-17d4699ac1f9',
    XIBMVendor : '70e23cd8-6031-489d-a8e0-8fc253b1180d',

    XIBMInternalProd : '65bdd6b2-ed53-480b-8d93-d01b0f96225b'
}
  