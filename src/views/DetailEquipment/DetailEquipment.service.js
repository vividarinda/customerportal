import axios from 'axios';
import Environment from '../Environment/Environment';

async function detailequipment(token, equipmentId) {
    const requestConfig = {
        url: `${Environment.baseInternal}/EM/api/equipment/` + equipmentId,
        headers: {
            "x-ibm-client-id": Environment.XIBMInternal,
            "Authorization": "Bearer " + token,
        }
    }
    const res = await axios(requestConfig);
    return res;
}

export default detailequipment;
