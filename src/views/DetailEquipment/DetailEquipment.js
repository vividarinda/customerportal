import React, { Component } from 'react';
import { Row, Col, Card, CardBody, Badge, TabPane, Alert, Button, Modal, ModalHeader, ModalFooter, ModalBody, UncontrolledTooltip } from 'reactstrap';
import { Bar, Doughnut } from 'react-chartjs-2';
import './DetailEquipment.style.css';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import MediaQuery from 'react-responsive';
import {AsyncStorage} from 'AsyncStorage';
import detailequipment from './DetailEquipment.service';
import {getDetailEquipmentAll, getEquipmentsByType} from '../../actions/ActionDetailEquip';
import Environment from '../Environment/Environment';
import {connect} from 'react-redux';

const doughnut = {
    datasets: [
        {
            data: [9, 3],
            backgroundColor: [
                '#7D64E9',
                '#DFE2EF',
            ],
            hoverBackgroundColor: [
                '#7D64E9',
                '#DFE2EF',
            ],
            borderWidth: 0,
        }],
    labels: [
        '9 Actual Working Hours',
        '3 Idling Hours',
    ]
};

const bar = {
    labels: ['17 SwingHours', '24 TravellingHours', '21 DiggingHours'],
    datasets: [
        {
            backgroundColor: '#7D64E9',
            borderWidth: 0,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [18, 25, 21],
        },
    ],
};
const mapStyles = {
    width: '100%',
    height: '100%',
};

class DetailEquipment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            persons: [],
            map: [],
            equipmentId: "1f1a8744-c405-47fb-1209-08d6a1f0a98b"
        };
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.setState({
            modal: !this.state.modal,
        });
    }
    getDetailEquipmentAll(){
      AsyncStorage.getItem('dataLogin').then((result) => {
        
        if (result){
          let resultparse = JSON.parse(result);
          const token = resultparse.accessToken;
          detailequipment(token, this.state.equipmentId).then(response => {
            console.log("masuk = "+JSON.stringify(response.data));
            this.setState({persons:response.data});
          });
          }
        })
    }

    render() {
        let month_names = [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];

          var ctsWED = (new Date(this.props.listDetailEquipment.warrantyEndDate));
          var warrantyEndDate = ctsWED.getDate() + ' ' + month_names[ctsWED.getMonth()] + ' ' + ctsWED.getFullYear();

          var ctsSLD = (new Date(this.props.listDetailEquipment.smrLastValueDate));
          var smrLastValueDate = ctsSLD.getDate() + ' ' + month_names[ctsSLD.getMonth()] + ' ' + ctsSLD.getFullYear();

        return (
            <div className="animated fadeIn background">
                <Row className="bgEquipment background">
                    <div className="col-12 col-s-12">
                        <div className="cardEM">
                            <div className="fontEM">Detail Status Equipment</div>
                        </div>
                    </div>
                </Row>
                <Row>
                    <div className="col-12 col-s-12 trafficstyle background">
                        <div>
                            <Row>
                                <div className="hiasan"></div>
                                <div className="stylelisteq">
                                    Unit Information
                                    </div>
                            </Row>
                            <CardBody>
                                <Row className="textvalue4">
                                    <div className="col-5">
                                        <img src={this.props.listDetailEquipment.imagePath} width="240vw" className="marginImage" />
                                    </div>
                                    <div className="col-7">
                                        <Row >
                                            <div className="col-6">
                                                <div className="garisabu2"></div>
                                                <div>
                                                    <div className="font1">
                                                        Model Number
                                                                </div>
                                                    <div className="font2">
                                                        <p>{this.props.listDetailEquipment.modelNumber}</p>
                                                    </div>
                                                    <div className="font1">
                                                        Serial Number
                                                                </div>
                                                    <div className="font3">
                                                        <p>{this.props.listDetailEquipment.serialNumber}</p>
                                                    </div>
                                                    <div className="font1">
                                                        Unit Code
                                                                </div>
                                                    <div className="font3">
                                                        <p>{this.props.listDetailEquipment.unitCode}</p>
                                                    </div>
                                                </div>
                                                <div className="col-s-5">
                                                    <Row>
                                                        <div className="garisabu2"></div>
                                                            <div>
                                                                <div className="font1">
                                                                    Warranty End Date
                                                                </div>
                                                                <div className="font3">
                                                                    <p>{this.state.persons.warrantyEndDate}</p>
                                                                    {/* { Date.prototype.getFormatDate = function() {
                                                                    const month_names = [
                                                                        'January', 'February', 'March', 'April', 'May', 'June', 
                                                                        'July', 'August', 'September', 'October', 'November', 'December'];
                                                                    var ctsSMR=(new Date(this.state.persons.warrantyEndDate));
                                                                    var formatSMR = ctsSMR.getDate() + ' ' + month_names[ctsSMR.getMonth()] + ' ' + ctsSMR.getFullYear();
                                                                    return(
                                                                        <p>{formatSMR}</p>
                                                                        );
                                                                    } */}
                                                                </div>
                                                                <div className="font4">
                                                                    <p>{this.state.persons.warrantyStatusString}</p>
                                                                </div>
                                                                <Row>
                                                                    <span class="badge badge-warning">SMR</span><br/>
                                                                    <div className="font5">
                                                                        <p>{this.state.persons.smrTotalInMinutes/60} hr</p>
                                                                    </div> 
                                                                </Row>
                                                                <Row>
                                                                    <span class="badge badge-warning">SMR DATE</span>
                                                                    <div className="font5">
                                                                        <p>{this.state.persons.smrLastValueDate}</p>
                                                                    </div> 
                                                                </Row>                          
                                                            </div>
                                                    </Row>
                                                </div>
                                            </div>
                                        </Row>
                                    </div>
                                </Row>
                            </CardBody>
                        </div>
                    </div>
                </Row>
                <Row>
                    <Col className="trafficstyle background">
                        <div>
                            <Row>
                                <div className="hiasan"></div>
                                <div className="stylelisteq">
                                    Working Status
                                    </div>
                            </Row>
                            <Row className="textvalue3">
                                <CardBody>
                                    <Row>
                                        <div className="col-4">
                                            <div className="cardnew">
                                                <CardBody className="pb-0">
                                                    <div className="textvalue1">
                                                        Fuel Consumption Per Day
                                                    </div>
                                                    <div className="imgg">
                                                        <img src={'../../assets/img/oval.png'} width="130" />
                                                        <div className="text">
                                                            <p>{this.props.listDetailEquipment.valueFuelConsumption} Liter</p>
                                                        </div>
                                                    </div>
                                                </CardBody>
                                            </div>
                                        </div>
                                        <div className="col-4">
                                            <div className="cardnew">
                                                <CardBody className="pb-0">
                                                    <div className="textvalue1">
                                                        Working Hours
                                                    </div>
                                                </CardBody>
                                                <div className="chart-wrapper">
                                                    {/* <Doughnut data={doughnut} />
                                                    <div className="textt">12</div>
                                                    <div className="text1">Total Working Hours</div> */}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-4">
                                            <div className="cardnew">
                                                <CardBody className="pb-0">
                                                    <div className="textvalue1">
                                                        Working Characteristics
                                                    </div>
                                                </CardBody>
                                                <div className="chart-wrapper">
                                                    {/* <Bar data={bar} /> */}
                                                </div>
                                            </div>
                                        </div>
                                    </Row>
                                </CardBody>
                            </Row>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col className="trafficstyle background">
                        <div>
                            <Row>
                                <div className="hiasan"></div>
                                <div className="stylelisteq">
                                    Caution Information
                                    </div>
                            </Row>
                            <CardBody>
                                <div className="cardcaution">
                                    <Row className="textvalue2">
                                        <div className="col-2 col-s-2">
                                            <img className="imgc" src={'../../assets/img/cautionIcon.png'} />
                                        </div>
                                        <div className="col-2 col-s-2">
                                            <div className="dtcaut">10-11-2019</div>
                                        </div>
                                        <div className="col-5 col-s-5">
                                            <Row>
                                                <div className="nmcaut">{this.props.listDetailEqCuation.description}</div>
                                                <img className="imgac" src={'../../assets/img/askicon.png'} onClick={this.toggle} />
                                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                                    <ModalHeader toggle={this.toggle}>
                                                        Engine Oil Pressure
                                                </ModalHeader>
                                                    <ModalBody>
                                                        <p>- Engine speed diturunkan ke low idling.</p>
                                                        <p>- Periksa level coolant.</p>
                                                        <p>- Jika level coolant rendah ikuti prosedur pengisian coolant.</p>                                                        <p>- Periksa Fin Radiator dimungkinkan ada kotoran yang menyumbat aliran udara.</p>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color="primary" onClick={this.toggle}>Ok</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </Row>
                                        </div>
                                        <div className="col-3 col-s-3">
                                            <div className="nmcaut"> Frequency 2</div>
                                        </div>
                                    </Row>
                                </div>
                                {/* <div className="cardcaution">
                                    <Row className="textvalue2">
                                        <div className="col-2 col-s-2">
                                            <img className="imgc" src={'../../assets/img/cautionIcon.png'} width="25" />
                                        </div>
                                        <div className="col-2 col-s-2">
                                            <div className="dtcaut">10-11-2019</div>
                                        </div>
                                        <div className="col-5 col-s-5">
                                            <Row>
                                                <div className="nmcaut">Radiator Coolant Level</div>
                                                <img className="imgac" src={'../../assets/img/askicon.png'} onClick={this.toggle} />
                                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                                    <ModalHeader toggle={this.toggle}>
                                                        Engine Oil Pressure
                                                    </ModalHeader>
                                                    <ModalBody>
                                                        <p>- Engine speed diturunkan ke low idling.</p>
                                                        <p>- Periksa level coolant.</p>
                                                        <p>- Jika level coolant rendah ikuti prosedur pengisian coolant.</p>
                                                        <p>- Periksa Fin Radiator dimungkinkan ada kotoran yang menyumbat aliran udara.</p>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color="primary" onClick={this.toggle}>Ok</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </Row>
                                        </div>
                                        <div className="col-3 col-s-3">
                                            <div className="nmcaut">Frequency 1</div>
                                        </div>
                                    </Row>
                                </div>
                                <div className="cardcaution">
                                    <Row className="textvalue2">
                                        <div className="col-2 col-s-2">
                                            <img className="imgc" src={'../../assets/img/cautionIcon.png'} width="25" />
                                        </div>
                                        <div className="col-2 col-s-2">
                                            <div className="dtcaut">10-11-2019</div>
                                        </div>
                                        <div className="col-5 col-s-5">
                                            <Row>
                                                <div className="nmcaut">Engine Oil Pressure</div>
                                                <img className="imgac" src={'../../assets/img/askicon.png'} onClick={this.toggle} />
                                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                                    <ModalHeader toggle={this.toggle}>
                                                        Engine Oil Pressure
                                                    </ModalHeader>
                                                    <ModalBody>
                                                        <p>- Engine speed diturunkan ke low idling.</p>
                                                        <p>- Periksa level coolant.</p>
                                                        <p>- Jika level coolant rendah ikuti prosedur pengisian coolant.</p>
                                                        <p>- Periksa Fin Radiator dimungkinkan ada kotoran yang menyumbat aliran udara.</p>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color="primary" onClick={this.toggle}>Ok</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </Row>
                                        </div>
                                        <div className="col-3 col-s-3">
                                            <div className="nmcaut">Frequency 4</div>
                                        </div>
                                    </Row>
                                </div>
                                <div className="cardcaution">
                                    <Row className="textvalue2">
                                        <div className="col-2 col-s-2">
                                            <img className="imgc" src={'../../assets/img/cautionIcon.png'} width="25" />
                                        </div>
                                        <div className="col-2 col-s-2">
                                            <div className="dtcaut">10-11-2019</div>
                                        </div>
                                        <div className="col-5 col-s-5">
                                            <Row>
                                                <div className="nmcaut">Air Cleaner Clogging</div>
                                                <img className="imgac" src={'../../assets/img/askicon.png'} onClick={this.toggle} />
                                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                                    <ModalHeader toggle={this.toggle}>
                                                        Engine Oil Pressure
                                                    </ModalHeader>
                                                    <ModalBody>
                                                        <p>- Engine speed diturunkan ke low idling.</p>
                                                        <p>- Periksa level coolant.</p>
                                                        <p>- Jika level coolant rendah ikuti prosedur pengisian coolant.</p>
                                                        <p>- Periksa Fin Radiator dimungkinkan ada kotoran yang menyumbat aliran udara.</p>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color="primary" onClick={this.toggle}>Ok</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </Row>
                                        </div>
                                        <div className="col-3 col-s-3">
                                            <div className="nmcaut">Frequency 3</div>
                                        </div>
                                    </Row>
                                </div> */}
                            </CardBody>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col className="trafficstyle background">
                        <div>
                            <Row>
                                <div className="hiasan"></div>
                                <div className="stylelisteq">
                                    Location Information
                                    </div>
                            </Row>
                            <Row className="textvalue4">
                                <CardBody>
                                    <Row className="font6">
                                        <span className="fa fa-circle fa-lg mt-4"></span>
                                        <div className="font7">Transmit</div>
                                    </Row>
                                    <Row>
                                        <div class="col-3 col-s-2" className="font8">
                                            Last Transmit<br />
                                            <span className="fa fa-calendar"> {this.state.persons.lastTransmitted}
                                            </span>
                                        </div>
                                        <div class="col-3 col-s-11" className="font8">
                                            Current Plant<br />
                                            <span className="LI">
                                                <p>{this.state.persons.plantDescription}</p>
                                            </span>
                                        </div>
                                        <div class="col-3 col-s-6">
                                            <span className="fa fa-clock-o"> 02:00:00</span>
                                        </div>
                                        <div class="col-3 col-s-6" className="LII">
                                            <span className="icon-location-pin"> {this.state.persons.transmittedPlantKabupaten}, {this.state.persons.transmittedPlantZipCode}
                                            </span>
                                        </div>
                                        <div class="col-1 col-s-6" className="LII">
                                            <span className="icon-share"></span>
                                            <a href="#" id="UncontrolledTooltipExample"> share</a>
                                            <UncontrolledTooltip placement="top" target="UncontrolledTooltipExample">
                                                <Button size="sm" className="btn-facebook btn-brand icon mr-1 mb-1"><i className="fa fa-facebook"></i></Button>
                                                <Button size="sm" className="btn-instagram btn-brand icon mr-1 mb-1"><i className="fa fa-instagram"></i></Button>
                                                <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1"><i className="fa fa-twitter"></i></Button>
                                            </UncontrolledTooltip>
                                        </div>
                                        <Row className="location">
                                            <div class="col-12 col-s-12" className="map">
                                                <Map google={this.props.google} zoom={8} style={mapStyles} initialCenter={{ lat: 47.444, lng: -122.176 }}>
                                                    <Marker position={{ lat: 48.00, lng: -122.00 }} />
                                                </Map>
                                            </div>
                                        </Row>
                                    </Row>
                                </CardBody>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = ({ ListHeadDetailEquipment }) => {
    const { listDetailEquipment,listDetailEqCuation,  error } = ListHeadDetailEquipment;
    return { listDetailEquipment,listDetailEqCuation,  error };
}
export default connect(mapStateToProps, { getDetailEquipment, getDetailEqCaution })(DetailEquipment);

// export default GoogleApiWrapper({
//     apiKey: ("AIzaSyDuQWwMvbkhtBHrGOJ1WRjj5tvbf0xWtzU")
//   })(DetailEquipment);
