import axios from 'axios';
import Environment from '../Environment/Environment';
import { async } from 'q';

async function home(token, brandCode) {
    const requestConfig = {
        url: `${Environment.baseInternal}/EM/api/equipment/dashboard`,
        headers: {
            "x-ibm-client-id": Environment.XIBMInternal,
            "Authorization": "Bearer " + token,
        }
    }
    const res = await axios(requestConfig);
    return res;
}

export default home;
