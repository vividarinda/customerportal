import React, { Component, lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import './Home.style.css';
import { getEquipmentAll, getEquipmentsByType } from '../../actions/ActionHome';
import { connect } from 'react-redux';
import Environment from '../Environment/Environment';

import {
  Badge, Button, ButtonDropdown, ButtonGroup, ButtonToolbar, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, CardFooter, CardHeader, Col, Carousel, CarouselCaption, CarouselControl,
  CarouselIndicators, CarouselItem, Container, Collapse, Dropdown, DropdownItem, DropdownMenu, DropdownToggle,
  Nav, NavItem, NavLink, Progress, Row, Table, Fade
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import MediaQuery from 'react-responsive';
import { AsyncStorage } from 'AsyncStorage';

const items = [
  {
    src: '../../assets/img/Header.png',
    altText: 'Slide 1',
    caption: 'Slide 1',
  },
  {
    src: '../../assets/img/Header.png',
    altText: 'Slide 2',
    caption: 'Slide 2',
  },
  {
    src: '../../assets/img/Header.png',
    altText: 'Slide 3',
    caption: 'Slide 3',
  },
];

class Home extends Component {

  constructor(props) {
    super(props);

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);

    this.toggle = this.toggle.bind(this);

    this.state = {
      activeIndex: 0,
      dropdownOpen: new Array(6).fill(false),
      persons: [],
      brandCode: "",
      setCol: new Array(6).fill('1'),
    };

  }

  componentWillMount() {
    this.props.getEquipmentAll();
    console.log(this.state.setCol[0] === '1')
    if (this.state.setCol[0] === '1') {
      this.props.getEquipmentsByType(this.state.brandCode = 'Brand01');
    }
    else if (this.state.setCol[0] === '2') {
      this.props.getEquipmentsByType(this.state.brandCode = 'Brand02');
    }
    else if (this.state.setCol[0] === '3') {
      this.props.getEquipmentsByType(this.state.brandCode = 'Brand03');
    }
    else if (this.state.setCol[0] === '4') {
      this.props.getEquipmentsByType(this.state.brandCode = 'Brand04');
    }
    else if (this.state.setCol[0] === '5') {
      this.props.getEquipmentsByType(this.state.brandCode = 'Brand05');
    }
    else {
      this.props.getEquipmentsByType(this.state.brandCode);
    }
  }

  setModel(result) {
    if (result != null) {
      return {

      }
    }
    return null
  }

  toggle(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => {
      return (index === i ? !element : false);
    });
    this.setState({
      dropdownOpen: newArray,
    });
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }


  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}>

          <img className="d-block w-100"
            src={item.src}
            alt={item.altText} />
        </CarouselItem>
      );
    });

    return (
      <main class="container-fluid">

        <Row className="posCar">
          <div class="col">
            <Card>
              <CardBody >
                <Carousel activeIndex={activeIndex} next={this.next} previous={this.previous}>
                  <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                  {slides}
                  <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                  <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
                </Carousel>
              </CardBody>
            </Card>
          </div>
        </Row>

        <Row className="bgEquipment background">
          <div class="col-s-3">
            <div className="cardEM">
              <div className="fontEM"> <img className="yell" src={'../../assets/img/box_yellow.png'} /> My Equipment </div>
            </div>
          </div>

          <div class="col-s-1" className="ddpos">
            <Dropdown isOpen={this.state.dropdownOpen[0]} toggle={() => {
              this.toggle(0);
            }}>
              <DropdownToggle caret className="background2">
                All Location
                  </DropdownToggle>
              <DropdownMenu>
                <DropdownItem> Location 1 </DropdownItem>
                <DropdownItem> Location 2 </DropdownItem>
                <DropdownItem> Location 3 </DropdownItem>
                <DropdownItem> Location 4 </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
        </Row>

        <Row>
          <div class="col-s-12" >
            <Row>
              <div class="col-s-4">
                <Row className="trafficstyle cardMar background">
                  <div class="col" setCol="1">
                    <div className="cardnew ">
                      <CardBody className="pb-0">
                        <div>
                          <img className="img-responsive" src={'../../assets/img/logoKomatsu.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">

                        <NavItem>
                          <NavLink
                            data={this.state.setCol[0] === '1'} >
                          </NavLink>
                        </NavItem>

                      </div>
                      <div className="fontUI">
                        Unit
                      </div>
                    </div>
                  </div>
                  <div class="col" setCol="2">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div >
                          <img className="img-responsive" src={'../../assets/img/udTrucksLogo.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">
                        <NavItem>
                          <NavLink
                            active={this.state.setCol[0] === '2'} >
                          </NavLink>
                        </NavItem>
                      </div>
                      <div className="fontUI">
                        Unit
                      </div>
                    </div>
                  </div>
                </Row>
              </div>

              <div class="col-s-4">
                <Row className="trafficstyle cardMar background">
                  <div class="col" setCol="3">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div>
                          <img className="img-responsive" src={'../../assets/img/logoScania.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">
                        30
                        </div>
                      <div className="fontUI">
                        Unit
                        </div>
                    </div>
                  </div>
                  <div class="col" setCol="4">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div>
                          <img className="img-responsive" src={'../../assets/img/bomagLogo.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">
                        30
                        </div>
                      <div className="fontUI">
                        Unit
                        </div>
                    </div>
                  </div>
                </Row>
              </div>

              <div class="col-s-4">
                <Row className="trafficstyle cardMar background">
                  <div class="col" setCol="5">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div>
                          <img className="img-responsive" src={'../../assets/img/tadanoCompanyLogo.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">
                        30
                            </div>
                      <div className="fontUI">
                        Unit
                            </div>
                    </div>
                  </div>
                  <div class="col" setCol="6">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div>
                          <img className="img-responsive" src={'../../assets/img/logoAll.png'} />
                        </div>
                      </CardBody>
                      <div className="paddingchart">
                        30
                        </div>
                      <div className="fontUI">
                        Unit
                        </div>
                    </div>
                  </div>
                </Row>
              </div>
            </Row>
          </div>
        </Row>

        <Row>
          <div class="col-s-6">
            <Row className="bgEquipment background">
              <div class="col">
                <div className="cardEM">
                  <div className="fontEM"> <img className="yell" src={'../../assets/img/box_yellow.png'} /> Notification </div>
                </div>
              </div>
            </Row>


            <Row >
              <div class="col">
                <div className="trafficstyle background">
                  <Row className="cardnew cardpos ">
                    <div>
                      <CardBody className="pb-0">
                        <img className="img-responsive2" src={'../../assets/img/Pending.png'} />
                      </CardBody>
                    </div>
                    <div>
                      <div className="fontUI2">24 Juli 2018 . 18:30 WIB</div>
                      <div className="textvalue">Pending Commisioning</div>
                      <div className="textvalue2">5 unit has not been commisioned.</div>
                    </div>
                  </Row>
                </div>
              </div>
            </Row>

            <Row>
              <div class="col">
                <div className="trafficstyle ">
                  <Row className="cardnew cardpos background">
                    <div>
                      <CardBody className="pb-0">
                        <img className="img-responsive2" src={'../../assets/img/iconChange2.png'} />
                      </CardBody>
                    </div>
                    <div>
                      <div className="fontUI2">24 Juli 2018 . 18:30 WIB</div>
                      <div className="textvalue">Interbranch Movement</div>
                      <div className="textvalue2">1 unit has equipment caution.</div>
                    </div>
                  </Row>

                </div>
              </div>
            </Row>

            <Row>
              <div class="col">
                <div className="trafficstyle ">
                  <Row className="cardnew cardpos background">
                    <div>
                      <CardBody className="pb-0">
                        <img className="img-responsive2" src={'../../assets/img/cautionIcon2.png'} />
                      </CardBody>
                    </div>
                    <div>
                      <div className="fontUI2">24 Juli 2018 . 18:30 WIB</div>
                      <div className="textvalue">Equipment Caution</div>
                      <div className="textvalue2">1 unit has equipment caution.</div>
                    </div>
                  </Row>

                </div>
              </div>
            </Row>

            <Row>
              <div class="col">
                <div className="trafficstyle ">
                  <Row className="cardnew cardpos background">
                    <div>
                      <CardBody className="pb-0">
                        <img className="img-responsive2" src={'../../assets/img/iconOrder.png'} />
                      </CardBody>
                    </div>
                    <div>
                      <div className="fontUI2">24 Juli 2018 . 18:30 WIB</div>
                      <div className="textvalue">New Order</div>
                      <div className="textvalue2">You have 5 new order.</div>
                    </div>
                  </Row>

                </div>
              </div>
            </Row>

            <Row>
              <div class="col">
                <div className="trafficstyle ">
                  <Row className="cardnew cardpos background">
                    <div>
                      <CardBody className="pb-0">
                        <img className="img-responsive2" src={'../../assets/img/iconPickup.png'} />
                      </CardBody>
                    </div>
                    <div>
                      <div className="fontUI2">24 Juli 2018 . 18:30 WIB</div>
                      <div className="textvalue">Ready To Pickup</div>
                      <div className="textvalue2">5 parts ready for pickup.</div>
                    </div>
                  </Row>

                </div>
              </div>
            </Row>

            <Row className="buttpos">
              <Button color="primary" size="lg" block>See All Notification</Button>
            </Row>

          </div>


          <div class="col-s-6">
            <Row className="colPos background">
              <div class="col">
                <div className="cardEM">
                  <div className="fontEM"> <img className="yell" src={'../../assets/img/box_yellow.png'} /> My Order </div>
                </div>
              </div>
            </Row>

            <Row className="mypos">
              <div class="col">
                <div className="trafficstyle2 background cardnew">
                  <CardBody className="pb-0">
                    <div className="ordTex">Total Order</div>
                    <div className="paddTex">30</div>
                    <div className="fonTex">List</div>
                    <Row>
                      <div class="col">
                        <Card className="bg-success">
                          <CardBody className="pb-0 mm">
                            <Row className="rgpadd">
                              <div class="col">
                                <div className="textvalue3">25</div>
                              </div>
                              <div class="col">
                                <div className="fontUI3">Open Order</div>
                              </div>
                            </Row>
                          </CardBody>
                        </Card>
                      </div>
                      <div class="col">
                        <Card className="bg-warning">
                          <CardBody className="pb-0 mm">
                            <Row className="rgpadd">
                              <div class="col">
                                <div className="textvalue3">5</div>
                              </div>
                              <div class="col">
                                <div className="fontUI3">Close Order</div>
                              </div>
                            </Row>
                          </CardBody>
                        </Card>
                      </div>
                    </Row>
                  </CardBody>
                </div>
              </div>
            </Row>

            <Row className="colPos background">
              <div class="col">
                <div className="cardEM">
                  <div className="fontEM"> <img className="yell" src={'../../assets/img/box_yellow.png'} /> My Ticket</div>
                </div>
              </div>
            </Row>

            <Row className="mypos">
              <div class="col" >
                <div className="trafficstyle2 background cardnew">
                  <CardBody className="pb-0">
                    <div className="ordTex">Total Ticket</div>
                    <div className="paddTex">30</div>
                    <div className="fonTex">List</div>

                    <Row>
                      <div class="col">
                        <Card className="bg-success">
                          <CardBody className="pb-0 mm">
                            <Row className="rgpadd">
                              <div class="col">
                                <div className="textvalue3">25</div>
                              </div>
                              <div class="col">
                                <div className="fontUI3">Open Order</div>
                              </div>
                            </Row>
                          </CardBody>
                        </Card>
                      </div>
                      <div class="col">
                        <Card className="bg-warning">
                          <CardBody className="pb-0 mm">
                            <Row className="rgpadd">
                              <div class="col">
                                <div className="textvalue3">5</div>
                              </div>
                              <div class="col">
                                <div className="fontUI3">Close Order</div>
                              </div>
                            </Row>
                          </CardBody>
                        </Card>
                      </div>
                    </Row>

                  </CardBody>
                </div>
              </div>
            </Row>

          </div>
        </Row>
      </main>
    );
  }
}

const mapStateToProps = ({ listDataHead }) => {
  const { listEquipment, listEquipmentType, error } = listDataHead;
  return { listEquipment, listEquipmentType, error };
}
export default connect(mapStateToProps, { getEquipmentAll, getEquipmentsByType })(Home);

