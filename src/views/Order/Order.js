import React, { Component, lazy, Suspense } from 'react';
import { Nav, NavItem, NavLink, TabContent, TabPane, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import './Order.style.css';
import { AsyncStorage } from 'AsyncStorage';
import { getOrder, getOrderSummary, getClosedOrder } from '../../actions/ActionOrder';
import { connect } from 'react-redux';
import StarRatingComponent from 'react-star-rating-component';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table
} from 'reactstrap';
import { ProgressBar } from 'react-bootstrap'
import Environment from '../Environment/Environment';
import axios from 'axios';

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataOrder: []
    };
  }

  componentWillMount() {
    this.props.getOrder();
    this.props.getOrderSummary();
    this.props.getClosedOrder();
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn background">
        <Row className="bgEquipment background">
          <Col>
            <div className="cardEM">
              <div className="fontEM">My Order</div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="trafficstyle background">
            <div>

              <div className="flex-container">
                <div className="hiasan"></div>
                <div className="stylelisteq">
                  Unit Information
                </div>
              </div>

              <CardBody>
                <Row>
                  <Col sm="2" xs="2">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div className="textvalue">Total Order</div>
                      </CardBody>
                      <div className="paddingchart">
                        {this.props.listOrderSummary.totalOrder}
                      </div>
                      <div className="fontUI">
                        List
                      </div>
                    </div>
                  </Col>
                  <Col sm="2" xs="2">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div className="textvalue">Open Order</div>
                      </CardBody>
                      <div className="paddingchart">
                        {this.props.listOrderSummary.openOrder}
                      </div>
                      <div className="fontUI">
                        Order
                      </div>
                    </div>
                  </Col>
                  <Col sm="2" xs="2">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div className="textvalue">Clode Order</div>
                      </CardBody>
                      <div className="paddingchart">
                        {this.props.listOrderSummary.closedOrder}
                      </div>
                      <div className="fontUI">
                        Order
                      </div>
                    </div>
                  </Col>
                  <Col sm="3" xs="3">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div className="textvalue">Send List Order</div>
                      </CardBody>
                      <div className="paddingchart">
                        82%
                      </div>
                      <div className="fontUI">
                        OTIF (On Time In Full)
                      </div>
                    </div>
                  </Col>
                  <Col sm="3" xs="3">
                    <div className="cardnew">
                      <CardBody className="pb-0">
                        <div className="textvalue">Send List Order</div>
                      </CardBody>
                      <div className="paddingchart">
                        <img src={'../../assets/img/sendMail.png'} height="90" width="" />
                      </div>
                      <div className="fontUI">
                        in CSV
                      </div>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="trafficstyle background">
            <div>
              <div className="orderList">
                <div className="flex-container">
                  <div className="hiasan"></div>
                  <div className="stylelisteq">
                    Order List
                </div>
                </div>
              </div>
              <div className="filtersort">
                <img src={'../../assets/img/iconFilter.png'} width="20" />
              </div>
              <div className="filterorder">
                <img src={'../../assets/img/iconShortByBold.png'} width="30" />

              </div>

              <CardBody>
                <Table responsive>
                  <thead className="listcolor">
                    <tr>
                      <th>PO NUMBER</th>
                      <th>SO DATE</th>
                      <th>ETA</th>
                      <th>ORDER ID</th>
                      <th>TOTAL ITEM</th>
                      <th>STATUS</th>
                      <th>FEEDBACK</th>
                      <th>AVAILABLE PERCENTAGE</th>
                    </tr>
                  </thead>

                  <tbody>
                    {
                      this.props.listOrder.map(person => {
                        let status = 'Open Order';
                        let month_names = [
                          'January', 'February', 'March', 'April', 'May', 'June',
                          'July', 'August', 'September', 'October', 'November', 'December'];

                        var ctsSO = (new Date(person.soDate));
                        var formatSO = ctsSO.getDate() + ' ' + month_names[ctsSO.getMonth()] + ' ' + ctsSO.getFullYear();

                        var ctsETA = (new Date(person.maxETA));
                        var formatETA = ctsETA.getDate() + ' ' + month_names[ctsETA.getMonth()] + ' ' + ctsETA.getFullYear();

                        return (
                          <tr key={person.poCustomer}>
                            <td>{person.poCustomer}</td>
                            <td>{formatSO}</td>
                            <td>{formatETA}</td>
                            <td>{person.soNumber}</td>
                            <td>{person.totalLineItem}</td>
                            <td>{status}</td>
                            <td>-</td>
                            <td><ProgressBar variant="warning" now={person.availabilityPercentage} label={`${person.availabilityPercentage}%`} /></td>
                          </tr>
                        )
                      })
                    }
                    {
                      this.props.listClosedOrder.map(close => {
                        let status = 'Closed Order';
                        let month_names = [
                          'January', 'February', 'March', 'April', 'May', 'June',
                          'July', 'August', 'September', 'October', 'November', 'December'];

                        var ctsSO = (new Date(close.deliveredDate));
                        var formatDD = ctsSO.getDate() + ' ' + month_names[ctsSO.getMonth()] + ' ' + ctsSO.getFullYear();

                        var ctsETA = (new Date(close.soDate));
                        var formatSO = ctsETA.getDate() + ' ' + month_names[ctsETA.getMonth()] + ' ' + ctsETA.getFullYear();
                        var emptyStarColor = "#D3D3D3"
                        return (
                          <tr key={close.poCustomer}>
                            <td>{close.poCustomer}</td>
                            <td>{formatDD}</td>
                            <td>{formatSO}</td>
                            <td>{close.soNumber}</td>
                            <td>-</td>
                            <td>{status}</td>
                            <td><StarRatingComponent
                              name="rate1"
                              starCount={5}
                              value={close.reviewStar}
                              emptyStarColor={emptyStarColor}
                            /></td>
                            <td><ProgressBar variant="warning" now={close.otifPercentage} label={`${close.otifPercentage}%`} /></td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </Table>
                <Pagination>
                  <PaginationItem>
                    <PaginationLink previous tag="button"></PaginationLink>
                  </PaginationItem>
                  <PaginationItem active>
                    <PaginationLink tag="button">1</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink tag="button">2</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink tag="button">3</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink tag="button">4</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink next tag="button"></PaginationLink>
                  </PaginationItem>
                </Pagination>
              </CardBody>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ listHeadOrder }) => {
  const { listOrder, listOrderSummary, listClosedOrder, error } = listHeadOrder;
  return { listOrder, listOrderSummary, listClosedOrder, error };
}
export default connect(mapStateToProps, { getOrder, getOrderSummary, getClosedOrder })(Order);
