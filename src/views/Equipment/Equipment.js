import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line, Doughnut, Chart } from 'react-chartjs-2';
import ReactDOM from 'react-dom';
import './Equipment.style.css';
import { getEquipmentallBefore, getEquipmentsByType, getEquipmentallPageAfter } from '../../actions/ActionEquipment';
import { connect } from 'react-redux';
import Environment from '../Environment/Environment';

import { Nav, NavItem, NavLink, TabContent, TabPane, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import { AsyncStorage } from 'AsyncStorage';

const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

var originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
  draw: function () {
    originalDoughnutDraw.apply(this, arguments);

    var chart = this.chart.chart;
    var ctx = chart.ctx;
    var width = chart.width;
    var height = chart.height;

    var fontSize = (height / 900).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "middle";

    var text = chart.config.data.text,
      textX = Math.round((width - ctx.measureText(text).width) / 2),
      textY = height / 2.5;

    ctx.fillText(text, textX, textY);
  }
});
class Equipment extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      activeTab: new Array(4).fill('1'),
      customerCode: "",
      plant: "",
      brandCode: "",
      type: "",
      dataType: [],
      dataCombine: [],
      page: 1,
    };

  }

  componentWillMount() {
    this.props.getEquipmentallBefore(this.state.page);
    this.props.getEquipmentallPageAfter(this.state.page + 1);

    this.props.getEquipmentsByType(this.state.brandCode);
  }

  componentWillReceiveProps() {

    if (this.state.activeTab[0] === '1') {
      return this.props.getEquipmentsByType(this.state.brandCode = '');
    }
    else if (this.state.activeTab[0] === '3') {
      return this.props.getEquipmentsByType(this.state.brandCode = 'Brand02');
    }
    else if (this.state.activeTab[0] === '4') {
      return this.props.getEquipmentsByType(this.state.brandCode = 'Brand03');
    }
    else if (this.state.activeTab[0] === '5') {
      return this.props.getEquipmentsByType(this.state.brandCode = 'Brand04');
    }
    else if (this.state.activeTab[0] === '6') {
      return this.props.getEquipmentsByType(this.state.brandCode = 'Brand05');
    }
    else if (this.state.activeTab[0] === '2') {
      return this.props.getEquipmentsByType(this.state.brandCode = 'Brand01');
    }
  }

  setModel(result) {
    if (result != null) {
      return {
        availablePercentage: this.calculatePercentage(this.props.listEquipmentType.availableCount, this.props.listEquipmentType.totalCount),
        notAvailablePercentage: this.calculatePercentage(this.props.listEquipmentType.notAvailableCount, this.props.listEquipmentType.totalCount),
        operatePercentage: this.calculatePercentage(this.props.listEquipmentType.operateCount, this.props.listEquipmentType.availableCount),
        notOperatePercentage: this.calculatePercentage(this.props.listEquipmentType.notOperateCount, this.props.listEquipmentType.availableCount),
        cautionPercentage: this.calculatePercentage(this.props.listEquipmentType.unitCautionCount, this.props.listEquipmentType.availableCount),
        tranmitPercentage: this.calculatePercentage(this.props.listEquipmentType.transmitCount, this.props.listEquipmentType.availableCount),
        lostTransmitPercentage: this.calculatePercentage(this.props.listEquipmentType.lostTransmitCount, this.props.listEquipmentType.availableCount),
      }
    }
    return null
  }

  calculatePercentage(count = 0, totalCount = 0) {
    return totalCount = count * 100 / totalCount;
  }

  lorem() {
    return 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.'
  }
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }
  toggle(tabPane, tab) {
    console.log("masuk")
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }

  tabPane() {

    const donatAvailable = {
      datasets: [
        {
          data: [
            [this.setModel(this.props.listEquipmentType).availablePercentage.toFixed(2)],
            [this.setModel(this.props.listEquipmentType).notAvailablePercentage.toFixed(2)]
          ],
          backgroundColor: [
            '#353535',
            '#00AAE8'
          ],
          hoverBackgroundColor: [
            '#353535',
            '#00AAE8'
          ],
        }],
      labels: [
        'Available',
        'Not Available'
      ],
      text: [this.setModel(this.props.listEquipmentType).availablePercentage.toFixed(2)] + " %"
    };

    const donatOperate = {
      datasets: [
        {
          data: [
            [this.setModel(this.props.listEquipmentType).operatePercentage.toFixed(2)],
            [this.setModel(this.props.listEquipmentType).notOperatePercentage.toFixed(2)]
          ],
          backgroundColor: [
            '#5ACC4D',
            '#E9445A'
          ],
          hoverBackgroundColor: [
            '#5ACC4D',
            '#E9445A'
          ],
        }],
      labels: [
        'Operate',
        'Not Operate'
      ],
      text: [this.setModel(this.props.listEquipmentType).operatePercentage.toFixed(2)] + " %"
    };

    const donatCaution = {
      datasets: [
        {
          data: [
            [this.setModel(this.props.listEquipmentType).cautionPercentage.toFixed(2)],
            100 - [this.setModel(this.props.listEquipmentType).cautionPercentage.toFixed(2)]
          ],
          backgroundColor: [
            '#FFCC00',
            '#E3E3E3'
          ],
          hoverBackgroundColor: [
            '#FFCC00',
            '#E3E3E3'
          ],
        }],
      labels: [
        'Caution',
        'Not Caution'
      ],
      text: [this.setModel(this.props.listEquipmentType).cautionPercentage.toFixed(2)] + " %"
    };

    const donatTransmit = {
      datasets: [
        {
          data: [
            [this.setModel(this.props.listEquipmentType).tranmitPercentage.toFixed(2)],
            [this.setModel(this.props.listEquipmentType).lostTransmitPercentage.toFixed(2)]
          ],
          backgroundColor: [
            '#5ACC4D',
            '#E9445A'
          ],
          hoverBackgroundColor: [
            '#5ACC4D',
            '#E9445A'
          ],
        }],
      labels: [
        'Transmit',
        'Not Transmit'
      ],
      text: [this.setModel(this.props.listEquipmentType).tranmitPercentage.toFixed(2)] + " %"
    };

    return (
      <>
        <TabPane tabId="1">
          {
            <Row>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TELEMETRY STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatAvailable}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
            </Col>
          </Row>
        }


      </TabPane>
      <TabPane tabId="2">
        {
          <Row>
            <Col className="col-3">
              <div className="cardnew">
                <CardBody className="pb-0">
                  <div className="textvalue">TELEMETRY STATUS</div>
                </CardBody>
                <div className="chart-wrapper paddingchart">
                  <Doughnut
                    data={donatAvailable}
                    options={{
                      responsive: true,
                      maintainAspectRatio: true,
                      legend: {
                        display: true,
                        position: "bottom"
                      }
                    }} />
                </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
            </Col>
          </Row>
        }
      </TabPane>
      <TabPane tabId="3">
        {
          <Row>
            <Col className="col-3">
              <div className="cardnew">
                <CardBody className="pb-0">
                  <div className="textvalue">TELEMETRY STATUS</div>
                </CardBody>
                <div className="chart-wrapper paddingchart">
                  <Doughnut
                    data={donatAvailable}
                    options={{
                      responsive: true,
                      maintainAspectRatio: true,
                      legend: {
                        display: true,
                        position: "bottom"
                      }
                    }} />
                </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
              </div>
            </Col>
          </Row>
        }
      </TabPane>
      <TabPane tabId="4">
        {
          <Row>
            <Col className="col-3">
              <div className="cardnew">
                <CardBody className="pb-0">
                  <div className="textvalue">TELEMETRY STATUS</div>
                </CardBody>
                <div className="chart-wrapper paddingchart">
                  <Doughnut
                    data={donatAvailable}
                    options={{
                      responsive: true,
                      maintainAspectRatio: true,
                      legend: {
                        display: true,
                        position: "bottom"
                      }
                    }} />
                </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
              </div>
            </Col>
          </Row>
        }
      </TabPane>
      <TabPane tabId="5">
        {
          <Row>
            <Col className="col-3">
              <div className="cardnew">
                <CardBody className="pb-0">
                  <div className="textvalue">TELEMETRY STATUS</div>
                </CardBody>
                <div className="chart-wrapper paddingchart">
                  <Doughnut
                    data={donatAvailable}
                    options={{
                      responsive: true,
                      maintainAspectRatio: true,
                      legend: {
                        display: true,
                        position: "bottom"
                      }
                    }} />
                </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
              </div>
            </Col>
          </Row>
        }
      </TabPane>
      <TabPane tabId="6">
        {
          <Row>
            <Col className="col-3">
              <div className="cardnew">
                <CardBody className="pb-0">
                  <div className="textvalue">TELEMETRY STATUS</div>
                </CardBody>
                <div className="chart-wrapper paddingchart">
                  <Doughnut
                    data={donatAvailable}
                    options={{
                      responsive: true,
                      maintainAspectRatio: true,
                      legend: {
                        display: true,
                        position: "bottom"
                      }
                    }} />
                </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">OPERATION STATUS</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatOperate}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">UNIT CAUTION</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatCaution}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
              </Col>
              <Col className="col-3">
                <div className="cardnew">
                  <CardBody className="pb-0">
                    <div className="textvalue">TRANSMIT RATIO</div>
                  </CardBody>
                  <div className="chart-wrapper paddingchart">
                    <Doughnut
                      data={donatTransmit}
                      options={{
                        responsive: true,
                        maintainAspectRatio: true,
                        legend: {
                          display: true,
                          position: "bottom"
                        }
                      }} />
                  </div>
                </div>
            </Col>
          </Row>
        }
      </TabPane>
      </>
    );
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    return (
      <div className="animated fadeIn background">
        <Row className="bgEquipment background">
          <Col className="col-s-12 col-12">
            <div className="cardEM">
              <div className="fontEM">Equipment Monitoring</div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="trafficstyle background col-s-12 col-12">
            <div>

              <div className="flex-container">
                <div className="hiasan"></div>
                <div className="stylelisteq">
                  Dashboard
              </div>
              </div>
              <Row className="paddingcontent">
                <Col className="col-s-12 col-12">
                  <Nav tabs class>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '1'}
                        onClick={() => { this.toggle(0, '1'); }}
                      >
                        <img src={'../../assets/img/iconAll.png'} width="30" height="17" className="paddingdashboard" />
                        All Unit
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '2'}
                        onClick={() => { this.toggle(0, '2'); }}
                      >
                        <img src={'../../assets/img/logoKomatsubr.png'} width="86" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '3'}
                        onClick={() => { this.toggle(0, '3'); }}
                      >
                        <img src={'../../assets/img/logoUD.png'} width="95" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '4'}
                        onClick={() => { this.toggle(0, '4'); }}
                      >
                        <img src={'../../assets/img/logoScania.png'} width="65" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '5'}
                        onClick={() => { this.toggle(0, '5'); }}
                      >
                        <img src={'../../assets/img/bomagLogo.png'} width="66" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        active={this.state.activeTab[0] === '6'}
                        onClick={() => { this.toggle(0, '6'); }}
                      >
                        <img src={'../../assets/img/logoTadano.png'} width="80" />
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab[0]}>
                    {this.tabPane()}
                  </TabContent>

                </Col>
              </Row>

            </div>
          </Col>
        </Row>
        <Row>
          <Col className="trafficstyle background col-s-12 col-12">
            <div>
              <div className="orderList">
                <div className="flex-container">
                  <div className="hiasan"></div>
                  <div className="stylelisteq">
                    Equipment List
              </div>
                </div>
              </div>
              <div className="filtersort">
                <img src={'../../assets/img/iconFilter.png'} width="20" />
              </div>
              <div className="filterorder">
                <img src={'../../assets/img/iconShortByBold.png'} width="30" />

              </div>
              <CardBody>
                <Table responsive>
                  <thead className="listcolor">
                    <tr>
                      <th>UNIT MODEL</th>
                      <th>SERIAL NUMBER</th>
                      <th>UNIT CODE</th>
                      <th>SMR</th>
                      <th>SMR DATE</th>
                      <th>WARRANTY END DATE</th>
                      <th>WARRANTY STATUS</th>
                      <th>LOCATION</th>
                      <th>STATUS</th>
                    </tr>
                  </thead>

                  <tbody>
                      {
                        this.props.listEquipment.map(person => {
                          let month_names = [
                            'January', 'February', 'March', 'April', 'May', 'June',
                            'July', 'August', 'September', 'October', 'November', 'December'];

                          var ctsSMR = (new Date(person.smrLastValueDate));
                          var formatSMR = ctsSMR.getDate() + ' ' + month_names[ctsSMR.getMonth()] + ' ' + ctsSMR.getFullYear();

                          var ctswarranty = (new Date(person.warrantyEndDate));
                          var formatwarranty = ctswarranty.getDate() + ' ' + month_names[ctswarranty.getMonth()] + ' ' + ctswarranty.getFullYear();

                          return (
                            <tr key={person.equipmentId}>
                              <td>{person.modelNumber}</td>
                              <td>{person.serialNumber}</td>
                              <td>{person.unitCode}</td>
                              <td>{person.smrTotalInMinutes / 60}</td>
                              <td>{formatSMR}</td>
                              <td>{formatwarranty}</td>
                              <td>{person.warrantyStatusString}</td>
                              <td>{person.plantDescription}</td>
                              <td>Status</td>
                            </tr>
                          )
                        })
                      }
                    {
                      this.props.listEquipmentAfter.map(after => {
                        let month_names = [
                          'January', 'February', 'March', 'April', 'May', 'June',
                          'July', 'August', 'September', 'October', 'November', 'December'];

                        var ctsSMR = (new Date(after.smrLastValueDate));
                        var formatSMR = ctsSMR.getDate() + ' ' + month_names[ctsSMR.getMonth()] + ' ' + ctsSMR.getFullYear();

                        var ctswarranty = (new Date(after.warrantyEndDate));
                        var formatwarranty = ctswarranty.getDate() + ' ' + month_names[ctswarranty.getMonth()] + ' ' + ctswarranty.getFullYear();

                        return (
                          <tr key={after.equipmentId}>
                            <td>{after.modelNumber}</td>
                            <td>{after.serialNumber}</td>
                            <td>{after.unitCode}</td>
                            <td>{after.smrTotalInMinutes / 60}</td>
                            <td>{formatSMR}</td>
                            <td>{formatwarranty}</td>
                            <td>{after.warrantyStatusString}</td>
                            <td>{after.plantDescription}</td>
                            <td>Status</td>
                          </tr>
                        )
                      })
                    }

                  </tbody>

                </Table>
                {/* <Pagination>
                  <PaginationItem>
                    <PaginationLink previous tag="button"></PaginationLink>
                  </PaginationItem>
                  <PaginationItem active>
                    <PaginationLink tag="button">1</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink next tag="button"></PaginationLink>
                  </PaginationItem>
                </Pagination> */}
              </CardBody>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = ({ listDataHead }) => {
  const { listEquipment, listEquipmentType, listEquipmentAfter, error } = listDataHead;
  return { listEquipment, listEquipmentType, listEquipmentAfter, error };
}
export default connect(mapStateToProps, { getEquipmentallBefore, getEquipmentsByType, getEquipmentallPageAfter })(Equipment);
