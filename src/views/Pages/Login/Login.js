import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {login} from './Login.service.js';
import './Login.style.css';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }
  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
  loginOnClick = event =>{
    this.setState({ submitted: true });
    const { username, password } = this.state;
    if (username && password) {
        login(username, password);
        this.props.history.push('/home')
    }
    
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <p><img src={'../../assets/img/logoUnitedTractor.png'}  width="300" /></p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" autoComplete="username" value={this.state.username} onChange={this.handleChange('username')}/>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" value={this.state.password} onChange={this.handleChange('password')}/>
                      </InputGroup>
                      <Row>
                        <Col xs="12" className="marginCol">
                          <Button color="warning" className="px-4" onClick={(event)=>{this.loginOnClick()}}>Login</Button> 
                          <p className="paddingRegister">Do not have account? <Link to="/register">Sign Up</Link></p>
                        </Col>
                      </Row>

                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white py-1 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <img src={'../../assets/img/login-background.jpg'} height="300"/>
                      {/* <Link to="/register">
                        <Button color="warning" className="mt-3" tabIndex={-1}>Register Now!</Button>
                      </Link> */}
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
