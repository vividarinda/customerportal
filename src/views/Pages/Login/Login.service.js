import axios from 'axios';
import Environment from '../../Environment/Environment';
import {AsyncStorage} from 'AsyncStorage';

export function login(username, password){

  let body = {
    username: username,
    password: password
  }
  return axios.post(Environment.baseInternalProd+'/api/login', body, {headers:{
    "x-ibm-client-id" : Environment.XIBMInternalProd}})
    .then((response => {
      let dataLogin = {
        userId : response['data']['userId'],
        userName : response['data']['userName'],
        firstName : response['data']['firstName'],
        lastName : response['data']['lastName'],
        email : response['data']['email'],
        customerCode : response['data']['customerCode'],
        roleLevel : response['data']['roleLevel'],
        regionLevel : response['data']['regionLevel'],
        accessToken : response['data']['tokenResponse']['accessToken'],
        identifyToken : response['data']['tokenResponse']['identifyToken'],
        tokenType : response['data']['tokenResponse']['tokenType'],
        refreshToken : response['data']['tokenResponse']['refreshToken'],
        errorDescription : response['data']['tokenResponse']['errorDescription'],
        expiresIn : response['data']['tokenResponse']['expiresIn']
   
      }
      
      AsyncStorage.setItem('dataLogin', JSON.stringify(dataLogin));
      console.log(response);
      return response;
    }))

}