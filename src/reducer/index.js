import {combineReducers} from 'redux';
import ListReducer from './EquipmentReducer.js';
import ListOrderReducer from './OrderReducer.js';
import ListDetailEqReducer from './DetailEquipmentReducer.js';

export default combineReducers ({
    listDataHead : ListReducer, 
    listHeadOrder : ListOrderReducer,
    ListHeadDetailEquipment : ListDetailEqReducer
})