import { combineReducers } from 'redux';
import { LIST_DETAIL_EQUIPMENT, LIST_DETAIL_EQ_CAUTION } from '../values/type.js';

const INITIAL_STATE = {
    listDetailEquipment: [],
    listDetailEqCuation: [],
    error: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_DETAIL_EQUIPMENT:
            return { ...state, error: '', listDetailEquipment: action.payload };
        case LIST_DETAIL_EQ_CAUTION:
            return { ...state, error: '', listDetailEqCuation: action.payload };
        default:
            return state;
    }
}