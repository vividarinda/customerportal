import { combineReducers } from 'redux';
import { LIST_SUCCESS, LIST_FAILED, LIST_SUCCESS_AFTER ,LIST, LIST_SUCCESS_BY_TYPE } from '../values/type.js';

const INITIAL_STATE = {
    listEquipment: [],
    listEquipmentType: [],
    listEquipmentAfter: [],
    error: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_SUCCESS:
            return { ...state, error: '', listEquipment: action.payload };
        case LIST_SUCCESS_AFTER:
            return { ...state, error: '', listEquipmentAfter: action.payload };
        case LIST_SUCCESS_BY_TYPE:
            return { ...state, error: '', listEquipmentType: action.payload };
        default:
            return state;
    }
}