import { combineReducers } from 'redux';
import { LIST_SUCCESS, LIST_FAILED, LIST, LIST_SUCCESS_BY_TYPE } from '../values/type.js';

const INITIAL_STATE = {
    listEquipment: [],
    listEquipmentType: [],
    error: ''
}

const homeReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_SUCCESS:
            return { ...state, error: '', listEquipment: action.payload };
        case LIST_SUCCESS_BY_TYPE:
            return { ...state, error: '', listEquipmentType: action.payload };
        default:
            return state;
    }
}

export default homeReducer;