import { combineReducers } from 'redux';
import { LIST_ORDER_SUCCESS, LIST_ORDER_SUMMARY, LIST_CLOSED_ORDER } from '../values/type.js';

const INITIAL_STATE = {
    listOrder: [],
    listOrderSummary: [],
    listClosedOrder: [],
    error: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_ORDER_SUCCESS:
            return { ...state, error: '', listOrder: action.payload };
        case LIST_ORDER_SUMMARY:
            return { ...state, error: '', listOrderSummary: action.payload };
        case LIST_CLOSED_ORDER:
            return { ...state, error: '', listClosedOrder: action.payload };
        default:
            return state;
    }
}