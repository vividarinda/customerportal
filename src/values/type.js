export const LIST_SUCCESS = 'list_success';
export const LIST_FAILED = 'list_failed';
export const LIST = 'list';
export const LIST_SUCCESS_BY_TYPE = 'list_success_by_type';
export const LIST_SUCCESS_AFTER = 'list_success_after';

export const LIST_ORDER_SUCCESS = 'list_order_success';
export const LIST_ORDER_SUMMARY = 'list_order_summary';
export const LIST_CLOSED_ORDER = 'list_closed_order';

export const LIST_DETAIL_EQUIPMENT = 'list_detail_equipment';
export const LIST_DETAIL_EQ_CAUTION = 'list_detail_eq_caution';
